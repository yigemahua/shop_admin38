import Vue from 'vue'
import VueRouter from 'vue-router'

// 引入组件
import Login from '../components/login/login.vue'
import Home from '../components/home/home.vue'
import Roles from '../components/roles/roles.vue'
import Users from '../components/users/users.vue'
import Rights from '../components/rights/rights.vue'
// use安装一下
Vue.use(VueRouter)

// 实例化路由
const router = new VueRouter({
    routes: [
        { path: '/', redirect: { name: 'login' } },
        { path: '/login', name: 'login', component: Login },
        {
            path: '/home',
            component: Home,
            children: [
                { path: '/users', component: Users },
                { path: '/rights', component: Rights },
                { path: '/roles', component: Roles }
            ]
        }
    ]
})

// 导航守卫
// to:目标路由对象
//from：来源路由对象
//next()下一步
router.beforeEach((to, from, next) => {
    if (to.path === '/login') {
        next()
    } else {
        // 再次判断是否登录过
        const token = localStorage.getItem('token')
        token ? next() : next('/login')
    }
})

// 导出路由
export default router