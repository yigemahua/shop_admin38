import axios from "axios";
/* eslint-disable */
export default {
    data() {
        return {
            userData: [{
                username: "王小虎",
                email: "1125052273@qq.com",
                mobile: "13328925165"
            }],
            total: 0,
            // 当前页
            pagenum: 1,
            //搜索文本
            searchText: "",
            //是否显示添加用户对话框
            dialogAddUserFormVisible: false,
            // 添加用户表单对象
            addUserForm: {
                username: "",
                password: "",
                email: "",
                mobile: ""
            },
            // 校验规则
            rules: {
                // 校验用户名
                username: [
                    // 判断是否输入的
                    { required: true, message: "请输入用户名", trigger: "blur" },
                    // 判断格式是否正确的
                    { min: 3, max: 5, message: "长度在3-5之间", trigger: "blur" }
                ],
                // 校验密码
                password: [
                    // 判断是否输入的
                    { required: true, message: "请输入密码", trigger: "blur" },
                    // 判断格式是否正确的
                    { min: 5, max: 10, message: "长度在5-10之间", trigger: "blur" }
                ],
                // 校验邮箱
                email: [
                    // 判断格式是否正确的
                    {
                        pattern: /^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,
                        message: "格式不正确",
                        trigger: "blur"
                    }
                ],
                // 校验手机
                mobile: [
                    // 判断是否输入的
                    { required: true, message: "请输入手机号", trigger: "blur" },
                    // 判断格式是否正确的
                    {
                        pattern: /^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/,
                        message: "手机号不正确",
                        trigger: "blur"
                    }
                ]
            }
        };
    },
    created() {
        this.loadUsersData();
    },
    methods: {
        // 获取用户数据
        async loadUsersData(pagenum = 1, query = "") {
            // 格式:axios.get(url,config(header,params))
            const url = "http://localhost:8888/api/private/v1/users";
            const config = {
                params: {
                    query,
                    pagenum,
                    pagesize: 2
                },
                headers: {
                    Authorization: localStorage.getItem("token")
                }
            };
            let res = await axios.get(url, config);
            // 把表格数据保存起来
            this.userData = res.data.data.users;
            // 把总个数数据绑定起来
            this.total = res.data.data.total;
            // 把当前页的数据保存起来
            this.pagenum = res.data.data.pagenum;
            // axios
            //     .get("http://localhost:8888/api/private/v1/users", {
            //         params: {
            //             query,
            //             pagenum,
            //             pagesize: 2
            //         },
            //         headers: {
            //             Authorization: localStorage.getItem("token")
            //         }
            //     })
            // .then(res => {
            //     console.log(res.data.data.users);
            // // 把表格数据保存起来
            // this.userData = res.data.data.users;
            // // 把总个数数据绑定起来
            // this.total = res.data.data.total;
            // // 把当前页的数据保存起来
            // this.pagenum = res.data.data.pagenum;
            // });
        },
        changeCurrent(currentPage) {
            this.loadUsersData(currentPage, this.searchText);
        },
        startQuery() {
            if (this.searchText.trim == 0) {
                return;
            }
            this.loadUsersData(1, this.searchText);
        },
        dialogAddUser() {
            this.dialogAddUserFormVisible = true;
        },
        clearForm() {
            this.addUserForm.username = "";
            this.addUserForm.password = "";
            this.addUserForm.email = "";
            this.addUserForm.mobile = "";
        },
        addUser() {
            axios
                .post("http://localhost:8888/api/private/v1/users", this.addUserForm, {
                    headers: {
                        Authorization: localStorage.getItem("token")
                    }
                })
                .then(res => {
                    console.log(res);
                    if (res.data.meta.status === 201) {
                        // 1.隐藏对话框
                        this.dialogAddUserFormVisible = false;
                        // 2.提示信息
                        this.$message({
                            message: "添加用户成功",
                            type: "success",
                            duration: 800
                        });
                        // 3.刷新一下
                        this.loadUsersData();
                        // 4.清空前面的表单
                        this.clearForm();
                    }
                });
        }
    }
};
