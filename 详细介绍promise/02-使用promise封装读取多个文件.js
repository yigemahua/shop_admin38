// 异步读取文件
const fs = require("fs");

// 封装promise
function ml_readF(filepath) {
    const p = new Promise((resolve, reject) => {
        fs.readFile(filepath, "utf-8", (err, data) => {
            if (err) {
                return reject("读取文件失败");
            }
            resolve(data);
        });
    });
    return p;
}

// 使用封装过的promise
ml_readF("./a.txt")
    .then(res1 => {
        console.log(res1);
        return ml_readF("./b.txt");
    })
    .then(res2 => {
        console.log(res2);
        return ml_readF("./c.txt");
    })
    .then(res3 => {
        console.log(res3);
    });
