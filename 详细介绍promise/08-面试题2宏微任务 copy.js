console.log("log1");

setTimeout(() => {
    console.log("timeout");
}, 0);

new Promise((resolve, reject) => {
    console.log("promise1");
    resolve();
}).then(res => {
    console.log("then1");
});

new Promise((resolve, reject) => {
    console.log("promise2");
    resolve();
}).then(res => {
    console.log("then2");
});

console.log("log2");

// 顺序
/*
lgo1
promise1
promise2
log2
then1
then2
timeout
*/

/* 宏任务和微任务
宏任务 ：script setTimeout
微任务：promise
执行顺序：
    宏任务=>微任务
*/
