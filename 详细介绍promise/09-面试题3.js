// 在axios中包装了 async 和await 对于大文件会出现等待问题，如何解决？
// 在套一个函数

// 如：下面这样容易出现等待问题

async function fn1() {
    let res1 = await axios.get(url);
    console.log(res1);
    let res2 = await axios.get(url);
    console.log(res2);
}
fn1();

// 解决
function fn() {
    async function fn1() {
        let res1 = await axios.get(url);
        console.log(res1);
    }
    async function fn2() {
        let res2 = await axios.get(url);
        console.log(res2);
    }
}
fn();
