// 异步读取文件
const fs = require("fs");
/*
1.async和await是es8提出来的
  作用：想编写同步代码的形式处理异步，处理的更加彻底
2.如何使用？
  async 修饰一个（内部有异步操作）的函数 格式：async+函数
  await 等待(一个异步处理的结果值)     格式 await +异步操作（promise类型）
3.注意点：
  1.async和await是成对出现的
  2.加不加await的结果不同，不加await就是promise，加await就是await，在promise前面能价await
  3.如果要抓取async和await的异常，使用try..catch(){}
  4.async必须要加在await最近的函数前面（就近原则）
*/
// 封装promise
function ml_readF(filepath) {
    const p = new Promise((resolve, reject) => {
        fs.readFile(filepath, "utf-8", (err, data) => {
            if (err) {
                return reject("读取文件失败");
            }
            resolve(data);
        });
    });
    return p;
}

// // 使用封装过的promise
// ml_readF("./a.txt").then(res1 => {
//     console.log(res1);
//     return ml_readF("./b.txt");
// });

function fn1() {
    async function fn() {
        try {
            let res1 = await ml_readF("./a.txt");

            console.log(res1);
        } catch (error) {
            console.log("说明读取失败");
        }
        let res2 = await ml_readF("./b.txt");
        let res3 = await ml_readF("./c.txt");
        console.log(res2);
        console.log(res3);
    }
}
fn1();
