// 异步读取文件
const fs = require("fs");

const p = new Promise((resolve, reject) => {
    fs.readFile("./a.txt", "utf-8", (err, data) => {
        if (err) {
            return reject("读取文件失败");
        }
        resolve(data);
    });
});
// 使用封装过的promise
p.then(res => {
    console.log(res);
}).catch(err => {
    console.log(err);
});
